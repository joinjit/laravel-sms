# SMS notification channels for Laravel

## About

The `laravel-sms` package allows you to send SMS messages via multiple service providers.
It creates a new notification channel which you can utilize by calling the `toSMS` function in your notification classes.
The package allows you to define custom rules to force sending SMS messages to certain countries using specific providers.

## Features

* Send SMS messages from Laravel using notification channels
* Ability to specify a custom Sender ID before sending a message
* Integrated with multiple SMS service providers
* Define rules based on country code and provider

### Available Providers
- [x] [Twilio](https://twilio.com)
- [x] [Infobip](https://infobip.com)
- [x] [Leblines](https://leblines.com)
- [x] [ProxiReach](https://proxireach.com)
- [x] [SMSGlobal](https://smsglobal.com)
- [x] Log

## Installation

You may install this package via Composer:

``` sh
composer require joinjit/laravel-sms
```

## Configuration

Publish the configuration file where you can setup your SMS providers

``` sh
php artisan vendor:publish --tag="laravel-sms"
```

## Advanced

In order for the package to detect the phone number, your `notifiable` should have a `phone` attribute on the Model.
The phone number should follow the E.164 international format (eg. +9613123456) or an `InvalidReceiverPhoneNumber` exception will be thrown.

To override the phone attribute on the `notifiable` Model, define a `routeNotificationForSms` function:

``` php
<?php

class User
{
    ...

    public function routeNotificationForSms()
    {
        return '+' . $this->phone_number;
    }

    ...
}
```
> In this example, the User model has a `phone_number` attribute instead of `phone` and does not include the plus sign for phone numbers which does not conform with E.164 standards.

## Usage

Create a new notification class using the `php artisan make:notification <notification_name>` command.

In your notification class, make sure to use the `Joinjit\LaravelSMS\SMSChannel` and `Joinjit\LaravelSMS\SMSMessage` classes. You should then specify the `SMSChannel::class` in your `via` method, and then implement a `toSMS` method that creates a new `SMSMessage`:

``` php
<?php

namespace App\Notifications;

use Joinjit\LaravelSMS\SMSChannel;
use Joinjit\LaravelSMS\SMSMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class WelcomeNotification extends Notification
{
    use Queueable;
    
    /**
     * The SMS message text.
     *
     * @var string
     */
    protected $text;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($text)
    {
        $this->text = $text;
    }

    /**
     * Get the notification channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return [SMSChannel::class];
    }

    /**
     * Get the SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SMSMessage
     */
    public function toSMS($notifiable)
    {
        return (new SMSMessage)
            ->sender(env('APP_NAME'))
            ->content($this->text);
    }
}
```

After creating your notification class, you may now utilize it as follows (given the model you are notifying already uses the `Notifiable` trait):

``` php
<?php

namespace App\Http\Controllers;

use App\User;
use App\Notifications\WelcomeNotification;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    ...

    /**
     * Send a welcome SMS to the user.
     *
     * @param Request $request
     * @return Response
     */
    public function sendWelcomeSms(Request $request)
    {
        ...

        // Notify the user
        $user->notify(new WelcomeNotification("Welcome to the app!"));

        ...
    }

    ...
}
```

## License

This library is licensed under the [MIT License](https://opensource.org/licenses/MIT)

