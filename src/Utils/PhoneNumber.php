<?php

namespace Joinjit\LaravelSMS\Utils;

use Exception;
use libphonenumber\PhoneNumber as PhoneNumberObject;
use libphonenumber\PhoneNumberType;
use libphonenumber\PhoneNumberUtil;

class PhoneNumber
{
    /**
     * The phone number object.
     *
     * @var PhoneNumberObject
     */
    protected $phone;

    /**
     * The phone validation utility.
     *
     * @var PhoneNumberUtil
     */
    protected $validator;

    /**
     * Create a new validator instance.
     *
     * @param String $phone
     *
     * @return void
     */
    public function __construct(String $phone)
    {
        // Initialize the validator instance
        $this->validator = PhoneNumberUtil::getInstance();

        // Try to parse the phone number
        try {
            $this->phone = $this->validator->parse($phone, null);
        }
        catch(Exception $e) {
            $this->phone = null;
        }
    }

    /**
     * Return the country code.
     *
     * @return int|null
     */
    public function getCountryCode()
    {
        return $this->phone ? $this->phone->getCountryCode() : null;
    }

    /**
     * Return if phone number is valid.
     *
     * @return bool
     */
    public function isValid()
    {
        return $this->phone ? $this->validator->isValidNumber($this->phone) : false;
    }

    /**
     * Return if phone number is a mobile number.
     *
     * @return bool
     */
    public function isMobile()
    {
        if($this->phone) {
            $type = $this->validator->getNumberType($this->phone);
            return $type == PhoneNumberType::MOBILE || $type == PhoneNumberType::FIXED_LINE_OR_MOBILE;
        }

        return false;
    }
}
