<?php

namespace Joinjit\LaravelSMS\Utils;

class Logger
{
  /**
   * Helper to output debug logs.
   *
   * @param mixed $output
   * @param array $context
   *
   * @return void
   */
    public static function debug($output, array $context = [])
    {
        if(config('sms.debug')) {
            logger()->debug($output . " " . json_encode($context));
        }
    }

    /**
     * Helper to output info logs.
     *
     * @param mixed $output
     *
     * @return void
     */
    public static function info($output)
    {
        logger()->info($output);
    }
}
