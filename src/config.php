<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Defaults
    |--------------------------------------------------------------------------
    |
    | Set the default driver and Alphanumeric Sender ID to use.
    |
    */

    'defaults' => [
        'driver' => env('SMS_DRIVER', 'log'),
        'sender_id' => env('SMS_DEFAULT_SENDER', '')
    ],

    /*
    |-------------------------------------------------------------------------
    | Sending rules
    |--------------------------------------------------------------------------
    |
    | Define rules to force certain countries to use specific drivers.
    | Countries that don't have rules will use the default driver.
    |
    */

    'rules' => [
    //      '961' => [
    //          'driver' => 'leblines',
    //          'sender_id' => 'Test'
    //      ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Drivers
    |--------------------------------------------------------------------------
    |
    | Here you can define the settings for each driver.
    |
    */

    'drivers' => [

        'twilio' => [
            'sender_id' => env('TWILIO_SENDER_ID', null),
            'account_id' => env('TWILIO_ACCOUNT_ID', null),
            'api_token' => env('TWILIO_API_TOKEN', null),
            'messaging_service_sid' => env('TWILIO_MESSAGING_SERVICE_SID', null),
        ],

        'infobip' => [
            'sender_id' => env('INFOBIP_SENDER_ID', null),
            'api_key' => env('INFOBIP_API_KEY', null),
        ],

        'leblines' => [
            'sender_id' => env('LEBLINES_SENDER_ID', null),
            'username' => env('LEBLINES_USERNAME', null),
            'password' => env('LEBLINES_PASSWORD', null),
        ],

        'proxireach' => [
            'sender_id' => env('PROXIREACH_SENDER_ID', null),
            'username' => env('PROXIREACH_USERNAME', null),
            'password' => env('PROXIREACH_PASSWORD', null),
        ],

        'smsglobal' => [
            'sender_id' => env('SMSGLOBAL_SENDER_ID', null),
            'username' => env('SMSGLOBAL_USERNAME', null),
            'password' => env('SMSGLOBAL_PASSWORD', null),
        ],

        'log' => [
            //
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Debugging
    |--------------------------------------------------------------------------
    |
    | Logs output from SMS providers as well as debugs SMS routing.
    |
    */

    'debug' => false,

    /*
    |--------------------------------------------------------------------------
    | Mocking
    |--------------------------------------------------------------------------
    |
    | Specify if you do not want to actually send the SMS through the provider.
    | Use this to simulate validation and rules without actually sending.
    |
    */

    'mock' => false,

];