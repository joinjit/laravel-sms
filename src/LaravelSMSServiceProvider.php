<?php

namespace Joinjit\LaravelSMS;

use Illuminate\Support\ServiceProvider;

class LaravelSMSServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config.php' => config_path('sms.php'),
        ], 'laravel-sms');
    }
}
