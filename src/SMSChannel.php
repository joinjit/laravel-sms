<?php

namespace Joinjit\LaravelSMS;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Notifications\Notification;
use Joinjit\LaravelSMS\Contracts\Driver;
use Joinjit\LaravelSMS\Drivers\Infobip;
use Joinjit\LaravelSMS\Drivers\Leblines;
use Joinjit\LaravelSMS\Drivers\Log;
use Joinjit\LaravelSMS\Drivers\ProxiReach;
use Joinjit\LaravelSMS\Drivers\SmsGlobal;
use Joinjit\LaravelSMS\Drivers\Twilio;
use Joinjit\LaravelSMS\Exceptions\DriverNotConfiguredException;
use Joinjit\LaravelSMS\Exceptions\InvalidMessageObject;
use Joinjit\LaravelSMS\Exceptions\InvalidReceiverPhoneNumber;
use Joinjit\LaravelSMS\Utils\Logger;
use Joinjit\LaravelSMS\Utils\PhoneNumber;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;

class SMSChannel
{
    /**
     * Send the given notification.
     *
     * @param mixed $notifiable
     * @param Notification $notification
     *
     * @return void|bool
     * @throws InvalidMessageObject
     * @throws InvalidReceiverPhoneNumber
     * @throws DriverNotConfiguredException
     * @throws GuzzleException
     * @throws TwilioException
     */
    public function send($notifiable, Notification $notification)
    {
        // Validate the receiver phone number
        $to = $this->getTo($notifiable);
        $phoneNumber = new PhoneNumber($to);
        if(!$to || !$phoneNumber->isValid() || !$phoneNumber->isMobile()) {
            throw new InvalidReceiverPhoneNumber();
        }

        // Get the SMS message object
        $message = $notification->toSMS($notifiable);
        if(is_string($message)) {
            $message = new SMSMessage($message);
        }
        if(!$message instanceof SMSMessage) {
            throw new InvalidMessageObject();
        }

        // Get the SMS driver to use
        $driver = $this->getDriver($phoneNumber);

        // Get the SMS Sender ID
        $from = $this->getFrom($phoneNumber, $message, $driver);

        Logger::debug('Sending SMS Message:', [
            'to' => $to,
            'from' => $from,
            'message' => $message->content,
            'driver' => $driver->getName()
        ]);

        return $driver->sendRequest($to, $from, $message->content);
    }

    /**
     * Get the phone number to send the notification to.
     *
     * @param mixed $notifiable
     *
     * @return mixed
     * @throws InvalidReceiverPhoneNumber
     */
    protected function getTo($notifiable)
    {
        if($notifiable->routeNotificationFor('sms')) {
            return $notifiable->routeNotificationFor('sms');
        }

        if(isset($notifiable->phone)) {
            return $notifiable->phone;
        }

        throw new InvalidReceiverPhoneNumber();
    }

    /**
     * Get the Sender ID to use when sending the notification.
     *
     * @param PhoneNumber $phoneNumber
     * @param SMSMessage $message
     * @param Driver $driver
     *
     * @return mixed
     */
    protected function getFrom(PhoneNumber $phoneNumber, SMSMessage $message, Driver $driver)
    {
        // Initialize the Sender ID
        $sender_id = isset($message->sender) && !is_null($message->sender) ? $message->sender : $driver->getSender();

        // Get the phone number country code
        $countryCode = $phoneNumber->getCountryCode();

        // Check if there is a Sender ID override for the phone country code
        if(config('sms.rules.' . $countryCode . '.sender_id')) {
            $sender_id = config('sms.rules.' . $countryCode . '.sender_id');
        }

        return $sender_id;
    }

    /**
     * Get the phone number to send the notification to.
     *
     * @param PhoneNumber $phoneNumber
     *
     * @return Driver $driver
     * @throws DriverNotConfiguredException
     * @throws ConfigurationException
     */
    protected function getDriver(PhoneNumber $phoneNumber)
    {
        // Initialize the driver
        $driver = config('sms.defaults.driver');

        // Get the phone number country code
        $countryCode = $phoneNumber->getCountryCode();

        // Check if there is a driver override for the phone country code
        if(config('sms.rules.' . $countryCode . '.driver')) {
            $driver = config('sms.rules.' . $countryCode . '.driver');
        }

        // Get config
        $config = config('sms.drivers.' . $driver);

        // Return the specific driver instance
        switch($driver) {
            case 'infobip':
                return new Infobip($config);
            case 'leblines':
                return new Leblines($config);
            case 'proxireach':
                return new ProxiReach($config);
            case 'twilio':
                return new Twilio($config);
            case 'smsglobal':
                return new SmsGlobal($config);
            default:
                return new Log(logger());
        }
    }
}
