<?php

namespace Joinjit\LaravelSMS\Contracts;

interface Driver
{
    /**
     * Send the SMS.
     *
     * @param String $to
     * @param String $from
     * @param String $message
     *
     * @return boolean
     */
    public function sendRequest(String $to, String $from, String $message): bool;

    /**
     * Get the Sender ID.
     *
     * @return String
     */
    public function getSender(): String;

    /**
     * Get the driver name.
     *
     * @return String
     */
    public function getName(): String;
}