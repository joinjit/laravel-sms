<?php

namespace Joinjit\LaravelSMS\Drivers;

use infobip\api\client\SendSingleTextualSms;
use infobip\api\configuration\ApiKeyAuthConfiguration;
use infobip\api\model\sms\mt\send\textual\SMSTextualRequest;
use Joinjit\LaravelSMS\Contracts\Driver;
use Joinjit\LaravelSMS\Exceptions\DriverNotConfiguredException;
use Joinjit\LaravelSMS\Utils\Logger;

class Infobip implements Driver
{
    /**
     * The Infobip Client.
     *
     * @var SendSingleTextualSms
     */
    protected $client;

    /**
     * Create a new driver instance.
     *
     * @param array $config
     *
     * @return void
     * @throws DriverNotConfiguredException
     */
    public function __construct(array $config)
    {
        // Check if driver is configured
        if(!array_key_exists('api_key', $config) || is_null($config['api_key'])) {
            throw new DriverNotConfiguredException();
        }

        // Initialize the client
        $this->client = new SendSingleTextualSms(new ApiKeyAuthConfiguration($config['api_key']));
    }

    /**
     * Send the SMS.
     *
     * @param String $to
     * @param String $from
     * @param String $message
     *
     * @return boolean
     */
    public function sendRequest(String $to, String $from, String $message): bool
    {
        if(!config('sms.mock')) {
            // Build the request
            $requestBody = new SMSTextualRequest();
            $requestBody->setTo($to);
            $requestBody->setFrom($from);
            $requestBody->setText($message);

            $this->client->execute($requestBody);
        }

        return true;
    }

    /**
     * Get the Sender ID.
     *
     * @return String
     */
    public function getSender(): String
    {
        return config('sms.drivers.infobip.sender_id') ?: config('sms.defaults.sender_id');
    }

    /**
     * Get the driver name.
     *
     * @return String
     */
    public function getName(): String
    {
        return 'Infobip';
    }
}