<?php

namespace Joinjit\LaravelSMS\Drivers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Joinjit\LaravelSMS\Contracts\Driver;
use Joinjit\LaravelSMS\Exceptions\DriverNotConfiguredException;

class Leblines implements Driver
{
    /**
     * The Guzzle Client.
     *
     * @var Client
     */
    protected $client;

    /**
     * The request query parameters.
     *
     * @var array
     */
    protected $params;

    /**
     * Create a new driver instance.
     *
     * @param array $config
     *
     * @return void
     * @throws DriverNotConfiguredException
     */
    public function __construct(array $config)
    {
        // Check if driver is configured
        if(!array_key_exists('username', $config) || !array_key_exists('password', $config) || is_null($config['username']) || is_null($config['password'])) {
            throw new DriverNotConfiguredException();
        }

        // Build the HTTP query params
        $this->params['user'] = $config['username'];
        $this->params['pass'] = $config['password'];

        // Initialize the client
        $this->client = new Client();
    }

    /**
     * Send the SMS.
     *
     * @param String $to
     * @param String $from
     * @param String $message
     *
     * @return boolean
     * @throws GuzzleException
     */
    public function sendRequest(String $to, String $from, String $message): bool
    {
        if(!config('sms.mock')) {
            // Add the options to params
            $this->params['to'] = $to;
            $this->params['senderid'] = $from;
            $this->params['message'] = $message;

            $this->client->post('http://leblines.com/api/?send&' . http_build_query($this->params));
        }

        return true;
    }

    /**
     * Get the Sender ID.
     *
     * @return String
     */
    public function getSender(): String
    {
        return config('sms.drivers.leblines.sender_id') ?: config('sms.defaults.sender_id');
    }

    /**
     * Get the driver name.
     *
     * @return String
     */
    public function getName(): String
    {
        return 'Leblines';
    }
}