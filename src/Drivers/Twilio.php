<?php

namespace Joinjit\LaravelSMS\Drivers;

use Joinjit\LaravelSMS\Contracts\Driver;
use Joinjit\LaravelSMS\Exceptions\DriverNotConfiguredException;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

class Twilio implements Driver
{
    /**
     * The Twilio Client.
     *
     * @var Client
     */
    protected $client;

    protected $messagingServiceSid;

    /**
     * Create a new driver instance.
     *
     * @param array $config
     *
     * @return void
     * @throws ConfigurationException
     * @throws DriverNotConfiguredException
     */
    public function __construct(array $config)
    {
        // Check if driver is configured
        if(!array_key_exists('account_id', $config) || !array_key_exists('api_token', $config) || is_null($config['account_id']) || is_null($config['api_token'])) {
            throw new DriverNotConfiguredException();
        }

        // Check if messaging service is defined
        if (array_key_exists('messaging_service_sid', $config)) {
            $this->messagingServiceSid = $config['messaging_service_sid'];
        }

        // Initialize the client
        $this->client = new Client($config['account_id'], $config['api_token']);
    }

    /**
     * Send the SMS.
     *
     * @param String $to
     * @param String $from
     * @param String $message
     *
     * @return boolean
     * @throws TwilioException
     */
    public function sendRequest(String $to, String $from, String $message): bool
    {
        if(!config('sms.mock')) {
            // Generate the payload
            $payload = [
                'body' => $message
            ];

            // Check if messaging service SID is specified
            if ($this->messagingServiceSid) {
                $payload['messagingServiceSid'] = $this->messagingServiceSid;
            } else {
                $payload['from'] = $from;
            }

            // Create the message
            $this->client->messages->create($to, $payload);
        }

        return true;
    }

    /**
     * Get the Sender ID.
     *
     * @return String
     */
    public function getSender(): String
    {
        return config('sms.drivers.twilio.sender_id') ?: config('sms.defaults.sender_id');
    }

    /**
     * Get the driver name.
     *
     * @return String
     */
    public function getName(): String
    {
        return 'Twilio';
    }
}