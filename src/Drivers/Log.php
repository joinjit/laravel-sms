<?php

namespace Joinjit\LaravelSMS\Drivers;

use Joinjit\LaravelSMS\Contracts\Driver;
use Psr\Log\LoggerInterface;

class Log implements Driver
{
    /**
     * The Logger Interface.
     *
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Create a new driver instance.
     *
     * @return void
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Send the SMS.
     *
     * @param String $to
     * @param String $from
     * @param String $message
     *
     * @return boolean
     */
    public function sendRequest(String $to, String $from, String $message): bool
    {
        $this->logger->info('SMS Message Sent', [
            'to' => $to,
            'from' => $from,
            'message' => $message
        ]);

        return true;
    }

    /**
     * Get the Sender ID.
     *
     * @return String
     */
    public function getSender(): String
    {
        return 'Log';
    }

    /**
     * Get the driver name.
     *
     * @return String
     */
    public function getName(): String
    {
        return 'Log';
    }
}