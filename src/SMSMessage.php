<?php

namespace Joinjit\LaravelSMS;

class SMSMessage
{
    /**
     * The SMS Sender ID.
     *
     * @var String|null
     */
    public $sender;

    /**
     * The SMS message content.
     *
     * @var String
     */
    public $content;

  /**
   * Create a new message instance.
   *
   * @param String $content
   */
    public function __construct(String $content = '')
    {
        $this->sender = null;
        $this->content = $content;
    }

    /**
     * Set the SMS Sender ID.
     *
     * @param mixed $sender
     *
     * @return $this
     */
    public function sender($sender)
    {
        // Only override sender if set.
        if($sender) {
            $this->sender = $sender;
        }

        return $this;
    }

    /**
     * Set the SMS message content.
     *
     * @param String $content
     *
     * @return $this
     */
    public function content(String $content)
    {
        $this->content = $content;
        return $this;
    }
}
